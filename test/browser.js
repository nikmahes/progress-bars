import jsdom from 'jsdom';

const { JSDOM } = jsdom;
const { document } = (new JSDOM('<html><body><progress-bars></progress-bars></body></html>')).window;
const window = document.defaultView;

Object.keys(window).forEach((key) => {
   	if (!(key in global)) {
     	global[key] = window[key];
   	}
});

//** These ones need to be done manually
global['Element'] = window.Element;
global['HTMLElement'] = window.HTMLElement;
global['WebComponents'] = function() {};
global['Node'] = window.Node;
global['Window'] = window;
global['Viewport'] = function() { return { setup: function() {} } }

//** Polyfill for values missing from JSDOM
window.getComputedStyle = function(el1, el2) {
   return [
     "transitionDuration"
   ]
}

global.document = document;
global.window = window;


// global.navigator = global.window.navigator;
// global.HTMLElement = window.HTMLElement;
// global.WebComponents = function() {};

// jsdom.env({
//   html: '<html><body></body></html>',
//   scripts: [
//     __dirname + '/../vendor/angular.min.js'
//   ],
//   features: {
//     FetchExternalResources: ["script"],
//     ProcessExternalResources: ["script"],
//   },
//   done: function(errors, window) {
//   }
// });
