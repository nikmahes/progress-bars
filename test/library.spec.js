import { expect } from 'chai';
import 'document-register-element';
import ProgressBars from '../src/index.js'

describe('mocha tests', function () {

  let progressBars = document.querySelector('progress-bars');

  it('has progress-bars as an element', function () {
    expect(progressBars.nodeName).eql('PROGRESS-BARS')
  });

  it('increses bar value when clicked', function () {
  	console.log('window.document : ', window.document);
  });

});
