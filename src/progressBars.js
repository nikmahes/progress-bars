const http = require('http');

export default class ProgressBars extends HTMLElement {

  changeBarValue(dataLimit) {
    let event = event ? event : arguments[1];
    let self = event.target || event.srcElement;
    let value = parseInt(self.textContent, 10);
    let id = document.querySelector('#pb-select').value;
    let barID = ['pb-bar', id].join('-');
    let bar = document.querySelector('#' + barID);
    let barValue = parseInt(bar.getAttribute('data-label'), 10);
    let spanID = ['pb-span', id].join('-');
    let span = document.querySelector('#' + spanID);

    barValue = barValue + value;
    barValue = barValue < 0 ? 0 : barValue;
    barValue > dataLimit ? bar.setAttribute('data-above-limit', 'true') :
      bar.setAttribute('data-above-limit', 'false');
    bar.value = barValue;
    bar.setAttribute('data-label', barValue + '%');
    span.textContent = barValue + '%';
  }

  doProcessing(data) {
    let self = this;
    let buttons = data.buttons;
    let bars = data.bars;
    let dataLimit = data.limit;
    let select = document.createElement('select');

    select.setAttribute('id', 'pb-select');

    bars.forEach((value, i) => {
      console.log('bar value : ', value);
      let paragraph = document.createElement('p');
      let progress = document.createElement('progress');
      let span = document.createElement('span');
      let option = document.createElement('option');
      let barID = ['pb-bar', i].join('-');
      let spanID = ['pb-span', i].join('-');
      let optionText = ['#progress', i + 1].join('');

      option.setAttribute('value', i);
      option.textContent = optionText;
      select.appendChild(option);
      progress.setAttribute('id', barID);
      progress.setAttribute('value', value);
      progress.setAttribute('max', 100);
      progress.setAttribute('data-label', value + '%');
      span.setAttribute('id', spanID);
      span.className = 'percentage';
      span.textContent = value + '%';
      paragraph.appendChild(progress);
      paragraph.appendChild(span);
      self.appendChild(paragraph);
    });

    self.appendChild(select);

    buttons.forEach((value, i) => {
      console.log('button value : ', value);
      let button = document.createElement('button');
      let buttonID = ['pb-button', i].join('-');

      button.setAttribute('id', buttonID);
      button.textContent = value;
      button.addEventListener('click', self.changeBarValue.bind(self, dataLimit));
      self.appendChild(button);
    });

    console.log('buttons : ', buttons);
    console.log('bars : ', bars);
    console.log('limits :', dataLimit);
  }

  createdCallback() {
    let self = this;

    http.get('http://pb-api.herokuapp.com/bars', (resp) => {
      let data = '';

      resp.on('data', (chunk) => {
        data += chunk;
      });
      resp.on('end', () => {
        self.doProcessing(JSON.parse(data));
      });
    }).on('error', (err) => {
      self.textContent = err.message;
      console.log('Error: ', err.message);
    });
  }
}
