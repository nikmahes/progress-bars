# Progress Bars

Vanilla JavaScript powered Progress Bars fetching bar counts and controls dynamically from an API Endpoint

[Example here](http://static.optus.com.au/pei/progress-bars-demo.ogv)

[Endpoint here](http://pb-api.herokuapp.com/bars)

# Requirements

 - Must read data from the endpoint
 - Multiple bars
 - One set of controls that can control each bar on the fly
 - Can't go under 0
 - Can go over limit (defined in API), but limit the bar itself and change its colour
 - Display usage amount, centered
 - Write tests for your code (hint: TDD strongly preferred)
 - Implement a responsive solution: testing it on mobile, tablet, etc. Getting it working nicely.
 - Animate the bar change, make sure it works well when you tap buttons quickly.
 - Version control (git)

# Bonus Points

 - Setting it up as a project
 - Setting up some automated tools
 - Linting, code quality, etc
 - JavaScript/CSS minification, packaging, etc
 - Using a CSS preprocessor like SASS/SCSS
 - Styling it to a production quality level

# Example structure from the endpoint

    {
        "buttons": [
            10,
            38,
            -13,
            -18
        ],
        "bars": [
            62,
            45,
            62
        ],
        "limit": 230
    }

# Contributors

Nikhil Maheshwari [ http://www.nikhilmaheshwari.com ]

# Demo
http://progress-bars.nikhilmaheshwari.com/